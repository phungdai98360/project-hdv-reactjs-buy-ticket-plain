import callApi from "./../../until/callerApi";
import { GETTURNPLANE, INSERTTURNPLANE,INSERTTICKET,UPDATETURNPLANE,DELETETURNPLANE } from "./../../config/index";
function* getTurnPlaneFromApi() {
  const res = yield callApi("GET", GETTURNPLANE, null);
  const turnPlane = res.data;
  return turnPlane;
}
function* addTurnPlaneFromApi(data) {
  const res = yield callApi("POST", INSERTTURNPLANE, data);
  const turnPlane = res.data;
  return turnPlane;
}
function* addTicketFromApi(data){
  const result = yield callApi("POST", INSERTTICKET, data);
  return result;

}
function* updateTurnPlane(data){
  const result=yield callApi("PUT",UPDATETURNPLANE,data);
  return result;
}
function* deleteTurnPlane(data){
  const result=yield callApi("PUT",DELETETURNPLANE,data);
  return result;
}
export const Api = {
  getTurnPlaneFromApi,
  addTurnPlaneFromApi,
  addTicketFromApi,
  updateTurnPlane,
  deleteTurnPlane
};
