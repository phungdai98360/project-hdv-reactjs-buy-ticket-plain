import { all } from "redux-saga/effects";
import { watchFetTurnPlane,watchAddTurnPlane,watchUpdateTurnPlane,watchDeletePlane } from "./actionSagaTurnPlane";
export default function* rootSaga() {
  yield all([watchFetTurnPlane(),watchAddTurnPlane(),watchUpdateTurnPlane(),watchDeletePlane()]);
}
