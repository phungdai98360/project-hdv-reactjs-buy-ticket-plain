import * as Types from "../../constain/index";
import { put, takeLatest, takeEvery, call } from "redux-saga/effects";
import { Api } from "./apis";
import { Alert } from "antd";
function* fetTurnPlane() {
  try {
    const data = yield Api.getTurnPlaneFromApi();
    yield put({
      type: Types.FET_TURN_PLANE_SUCCESS,
      data: data,
    });
  } catch (error) {
    console.log(error);
  }
}
var createNumber=()=>{
  return Math.floor(Math.random() * 100) +""+Math.floor(Math.random() * 100)+Math.floor(Math.random() * 100);
}
function* addTurnPlane(action) {
  try {
    const data=yield call(Api.addTurnPlaneFromApi,action.data)
    console.log(data);
    if(data)
    {
      for(let i=0;i<50;i++)
      {
        let params={
          mave: data.macb + createNumber(),
          soghe:"A"+i,
          macb:data.macb,
          gia:1500000
        }
        const result = yield call(Api.addTicketFromApi, params);
        console.log(result);
      }
      alert("Thêm thành công");
      yield put({
        type:Types.FET_TURN_PLANE,
      })
    }
  } catch (error) {
    console.log(error)
  }
}
function* updateTurnPlane(action){
  try {
    const result=yield call(Api.updateTurnPlane,action.data);
    console.log(result);
    if(result)
    {
      alert("Cập nhật thành công")
    }
    else{
      alert("Đã có lỗi")
    }
    yield put({
      type:Types.FET_TURN_PLANE,
    })
  } catch (error) {
    console.log(error)
  }
}
function* deleteTurnPlane(action){
  try {
    const result=yield call(Api.deleteTurnPlane,action.data);
    console.log(result);
    if(result){
      alert("Xóa thành công");
      yield put({
        type:Types.DELETE_TURN_PLANE_SUCCESS,
        data:result.data
      })
    }
  } catch (error) {
    console.log(error)
  }
}
export function* watchFetTurnPlane() {
  yield takeLatest(Types.FET_TURN_PLANE, fetTurnPlane);
}
export function* watchAddTurnPlane(){
  yield takeLatest(Types.ADD_TURN_PLANE,addTurnPlane)
}
export function* watchUpdateTurnPlane(){
  yield takeLatest(Types.UPDATE_TURN_PLANE,updateTurnPlane);
}
export function* watchDeletePlane(){
  yield takeLatest(Types.DELETE_TURN_PLANE,deleteTurnPlane);
}
