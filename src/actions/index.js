import * as Types from "./../constain/index";
import callApi from "./../until/callerApi";
import { GETALLAIRPORT } from "./../config/index";
//get all airport
export const actFetAirport = (airPort) => {
  return {
    type: Types.FET_AIRPORT,
    airPort,
  };
};
export const actFetAirportRequest = () => {
  return async (dispatch) => {
    return await callApi("GET", GETALLAIRPORT, null).then((res) => {
      dispatch(actFetAirport(res.data));
      console.log(res.data);
    });
  };
};
