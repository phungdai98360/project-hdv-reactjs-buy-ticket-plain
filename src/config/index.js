const API_URL = "http://datvemaybay.somee.com/api/";
//const API_URL = 'http://localhost:1337/api/';
export const GETALLAIRPORT = API_URL + "san-bay/get-all";
export const GETALLCHUYENBAY = API_URL + "chuyen-bay/get-by-query";
export const BOOKEDTICKET = API_URL + "ve/ticket-booking";
export const MYTICKET = API_URL + "ve/look-up-ticket";
export const GETTURNPLANE = API_URL + "chuyen-bay/get-all";
export const INSERTTURNPLANE = API_URL + "chuyen-bay/insert";
export const INSERTTICKET = API_URL + "ve/insert?";
export const UPDATETURNPLANE=API_URL+"chuyen-bay/update";
export const DELETETURNPLANE=API_URL+"chuyen-bay/delete";
export const GETALLPLANE=API_URL+"may-bay/get-all";

