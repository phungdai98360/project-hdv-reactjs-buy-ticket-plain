import { combineReducers } from "redux";
import listAirport from "./list-all-airport";
import listTurnPlane from "./turn_plane";
const appReducers = combineReducers({
  listAirport,
  listTurnPlane,
});
export default appReducers;
