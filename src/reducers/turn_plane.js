import * as Types from "./../constain/index";
const initialState = [];
const listTurnPlane = (state = initialState, action) => {
  switch (action.type) {
    case Types.FET_TURN_PLANE:
      return [...state];
    case Types.FET_TURN_PLANE_SUCCESS:
      state = action.data;
      return [...state];
    case Types.ADD_TURN_PLANE_SUCCESS:
      return [...state];
    case Types.DELETE_TURN_PLANE_SUCCESS:
         let data=action.data
         let index=state.findIndex(s=>s.macb===data.macb);
         state.splice(index,1);
         return [...state];
    default:
      return [...state];
  }
};
export default listTurnPlane;
