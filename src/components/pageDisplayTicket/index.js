import React, { Component } from "react";
import {
  Layout,
  Row,
  Col,
  Icon,
  Modal,
  Button,
  Tag,
  Table,
  Input,
} from "antd";
import "./index.scss";
import { GETALLCHUYENBAY, GETALLAIRPORT,BOOKEDTICKET } from "./../../config/index";
import callApi from "./../../until/callerApi";
const { Content } = Layout;

class PageDisplayTicket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listTicket: [],
      visible: false,
      inforChuyenbay: [],
      cmnd:"",
      visibleSuccess:false,
      inforTicket:[],
      
    };
  }
  fomatNgay(ngay) {
    let fmngay = "";
    if (ngay < 10) {
      fmngay = "0" + ngay;
    } else {
      fmngay = "" + ngay;
    }
    return fmngay;
  }
  async componentDidMount() {
    let { match } = this.props;
    let arr = [];
    let { airplanefrom, airplaneto, datefrom, dateto } = match.params;
    let url =
      GETALLCHUYENBAY +
      "?diemDi=" +
      airplanefrom +
      "&diemDen=" +
      airplaneto +
      "&ngayDi=" +
      datefrom;
    await callApi("GET", url, null).then((res) => {
      //console.log(res.data[0].gioidi.;
      let gio = "";
      let phut = "";
      let gioden = "";
      let phutden = "";
      for (let i = 0; i < res.data.length; i++) {
        gio =
          res.data[i].gioidi < 10
            ? "0" + res.data[i].gioidi
            : res.data[i].gioidi;
        phut =
          res.data[i].phutdi < 10
            ? "0" + res.data[i].phutdi
            : res.data[i].phutdi;
        gioden =
          res.data[i].gioden < 10
            ? "0" + res.data[i].gioden
            : res.data[i].gioden;
        phutden =
          res.data[i].phutden < 10
            ? "0" + res.data[i].phutden
            : res.data[i].phutden;
        arr.push({
          idChuyenBay: res.data[i].macb,
          timeStart: gio + ":" + phut,
          timeEnd: gioden + ":" + phutden,
          price: res.data[i].giave === null ? 500000 : res.data[i].giave,
          cityFrom: res.data[i].sbdi.tp + "(" + res.data[i].sbdi.masb + ")",
          cityTo: res.data[i].sbden.tp + "(" + res.data[i].sbden.masb + ")",
          ngaydi:
            this.fomatNgay(res.data[i].ngaydi) +
            "-" +
            this.fomatNgay(res.data[i].thangdi) +
            "-" +
            this.fomatNgay(res.data[i].namdi),
        });
      }
      this.setState(
        {
          listTicket: arr,
        },
        () => console.log(this.state.listTicket)
      );
    });
    //console.log(airplanefrom);
  }
  showModal = (chuyenbay) => {
    console.log(chuyenbay);
    this.setState({
      visible: true,
      inforChuyenbay: chuyenbay,
    });
  };

  handleOk = async (e) => {
    let params={
      cmnd:this.state.cmnd,
      thanhtien:this.state.inforChuyenbay.price,
      macb:this.state.inforChuyenbay.idChuyenBay
    }
     await callApi("POST",BOOKEDTICKET,params).then(res=>{
       console.log(res.data);
       //alert("Đặt vé thành công");
       this.setState({
        visible: false,
        inforTicket:res.data,
        visibleSuccess:true
      });
     })
  };

  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleOkSucces=(e)=>{
    this.setState({
      visibleSuccess: false,
    });
  }
  handleCancelSuccess = (e) => {
    console.log(e);
    this.setState({
      visibleSuccess: false,
    });
  };
  onCmtnd=(e)=>{
      this.setState({
        cmnd:e.target.value
      })
  }
  render() {
    let { inforChuyenbay,inforTicket } = this.state;
    const columns = [
      {
        title: "Chuyen bay",
        dataIndex: "idChuyenBay",
        key: "idChuyenBay",
      },
      {
        title: "Khoi hanh",
        dataIndex: "timeStart",
        key: "timeStart",
      },
      {
        title: "Den",
        dataIndex: "timeEnd",
        key: "timeEnd",
      },
      {
        title: "Gia",
        dataIndex: "price",
        key: "price",
      },
      {
        title: "Chi tiet",
        key: "idChuyenBay",
        render: (idChuyenBay) => (
          <span style={{ position: "relative" }}>
            <Button onClick={() => this.showModal(idChuyenBay)} type="danger">
              Chon
            </Button>
          </span>
        ),
      },
    ];
   
    return (
      <div>
        {this.state.listTicket.length <= 0 ? (
          <div
            style={{ textAlign: "center", alignItems: "center", height: 500 }}
          >
            <Icon
              style={{ paddingTop: 200, fontSize: 40, color: "#F04D24" }}
              type="loading"
            />
          </div>
        ) : (
          <Content style={{ margin: "0 100px", marginTop: 50 }}>
            <div style={{ paddingBottom: 20 }}>
              <div>
                Chuyến bay từ{" "}
                <strong style={{ color: "red" }}>
                  {this.state.listTicket[0].cityFrom}{" "}
                </strong>
                đi{" "}
                <strong style={{ color: "red" }}>
                  {this.state.listTicket[0].cityTo}
                </strong>
              </div>
              <div>
                Bay ngày <strong>{this.state.listTicket[0].ngaydi}</strong>
                <br />
                <i>Giá vé đã bao gồm thuế và phí</i>
              </div>
            </div>
            <Table columns={columns} dataSource={this.state.listTicket}></Table>
            <Modal
              title="Xác nhận đặt vé"
              visible={this.state.visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
            >
              <p>Chuyến bay : {inforChuyenbay.idChuyenBay}</p>
              <p>Từ : {inforChuyenbay.cityFrom}</p>
              <p>Đến : {inforChuyenbay.cityTo}</p>
              <p>Ngày đi: {inforChuyenbay.ngaydi}</p>
              <p>Giờ đi : {inforChuyenbay.timeStart}</p>

              <p>Giá vé: {inforChuyenbay.price}</p>
              <div>
              <label>Nhập chứng minh thư của bạn</label>
                <Input type="number" onChange={this.onCmtnd}/>
              </div>
            </Modal>
            <Modal
              title="Đặt vé thành công"
              visible={this.state.visibleSuccess}
              onOk={this.handleOkSucces}
              onCancel={this.handleCancelSuccess}
            >
              <p>Mã vế : {inforTicket.mave}</p>
              <p>Chuyến bay : {(inforTicket&&inforTicket.chuyenbay)?inforTicket.chuyenbay.macb:""}</p>
              <p>Từ : {inforChuyenbay.cityFrom}</p>
              <p>Đến : {inforChuyenbay.cityTo}</p>
              <p>Ngày đi: {(inforTicket && inforTicket.chuyenbay)?inforTicket.chuyenbay.ngaydi:"no data"}-{(inforTicket && inforTicket.chuyenbay)?(inforTicket.chuyenbay.thangdi<10?"0"+inforTicket.chuyenbay.thangdi:inforTicket.chuyenbay.thangdi):""}-{(inforTicket && inforTicket.chuyenbay)?inforTicket.chuyenbay.namdi:""}</p>
              <p>Giờ đi : {(inforTicket && inforTicket.chuyenbay)?(inforTicket.chuyenbay.gioidi<10?"0"+inforTicket.chuyenbay.gioidi:inforTicket.chuyenbay.gioidi):""}:{(inforTicket && inforTicket.chuyenbay)?(inforTicket.chuyenbay.phutdi<10?"0"+inforTicket.chuyenbay.phutdi:inforTicket.chuyenbay.phutdi):""}</p>

              <p>Giá vé: {(inforTicket&& inforTicket.hoadon)?inforTicket.hoadon.thanhtien:"0"}</p>
              
            </Modal>
          </Content>
        )}
      </div>
    );
  }
}

export default PageDisplayTicket;

