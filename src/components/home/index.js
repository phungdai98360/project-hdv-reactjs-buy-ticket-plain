import React, { Component } from "react";
import BookingTicket from "./../bookingticket/index";
import ContentKM from "./../contents/index";
class Home extends Component {
  render() {
    return (
      <div>
        <BookingTicket history={this.props.history} />
        <ContentKM />
      </div>
    );
  }
}

export default Home;
