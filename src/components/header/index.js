import React, { Component } from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import './index.scss';
import {Link} from "react-router-dom";
const { Header, Content, Footer } = Layout;

export default class HeaderLayout extends Component {
 
  render() {
    return (
        <Header className="header">
          <div className="logo">
            <h3>PTITALINE</h3>
          </div>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['1']}
            style={{ lineHeight: '47px' }}
          >
            <Menu.Item key="1"><Link to="/">Trang chủ</Link></Menu.Item>
            <Menu.Item key="2">Ptit khuyến mãi</Menu.Item>
            <Menu.Item key="3">Khách sạn</Menu.Item>
            <Menu.Item key="4"><Link to="/my-ticket">Chuyến bay của tôi</Link></Menu.Item>
            <Menu.Item key="5">Liên hệ</Menu.Item>
            <Menu.Item key="6"><Link to="/admin">Đăng nhập</Link></Menu.Item>
          </Menu>
        </Header>
    );
  }
}
