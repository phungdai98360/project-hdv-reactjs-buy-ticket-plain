import React, { Component } from "react";
import { Layout, Row, Col, Icon } from "antd";
import "./index.scss";
const { Content } = Layout;
const listImage = [
  "https://znews-photo.zadn.vn/w660/Uploaded/cqxrcajwp/2014_06_29/1.jpg",
  "https://poliva.vn/wp-content/uploads/2018/08/ho-guom-ha-noi.jpg",
  "https://data.voh.com.vn//uploads/Image/2016/06/14/193240kinhte140616.jpg",
  "https://znews-photo.zadn.vn/w660/Uploaded/cqxrcajwp/2014_06_29/1.jpg",
  "https://poliva.vn/wp-content/uploads/2018/08/ho-guom-ha-noi.jpg",
  "https://data.voh.com.vn//uploads/Image/2016/06/14/193240kinhte140616.jpg",
];
export default class ContentKM extends Component {
  render() {
    return (
      <Content style={{ padding: "0 100px" }}>
        <div style={{ background: "#fff", paddingTop: 24, minHeight: 280 }}>
          <Row>
            <Col xs={12} md={6}>
              <div className="divIcon">
                <Icon className="iconPhone" type="phone" />
                <h2 className="textIcon">Hỗ trợ khách hàng 24/7</h2>
              </div>
            </Col>
            <Col xs={12} md={6}>
              <div className="divIcon">
                <Icon className="iconPhone" type="idcard" />
                <h2 className="textIcon">Giá vé tốt nhất</h2>
              </div>
            </Col>
            <Col xs={12} md={6}>
              <div className="divIcon">
                <Icon className="iconPhone" type="shop" />
                <h2 className="textIcon">Dịch vụ đa dạng</h2>
              </div>
            </Col>
            <Col xs={12} md={6}>
              <div className="divIcon">
                <Icon className="iconPhone" type="car" />
                <h2 className="textIcon">Giao vé miễn phí</h2>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="divHotline">
                <Row>
                  <Col xs={12} md={6}>
                    <h1 className="textHotline">Vé PTIT - Giá rẻ mỗi ngày</h1>
                  </Col>
                  <Col xs={0} md={6}></Col>
                  <Col xs={0} md={6}></Col>
                  <Col xs={12} md={6}>
                    <h1 className="textPhone">Vé PTIT - Giá rẻ mỗi ngày</h1>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="divSaleTicket">
                <h2>Vé máy bay gia rẻ</h2>
              </div>
            </Col>
          </Row>
          <div className="slide-image">
            <Row gutter={[16, 24]}>
              {listImage.map((lm, index) => {
                return (
                  <Col key={index} xs={24} md={12} lg={8}>
                    <div style={{backgroundImage:"url('"+lm+"')"}} className="divImageSale"></div>
                  </Col>
                );
              })}
            </Row>
          </div>
        </div>
      </Content>
    );
  }
}
