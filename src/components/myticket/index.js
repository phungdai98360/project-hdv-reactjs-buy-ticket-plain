import React, { Component } from "react";
import { Row, Col, Input, Button, Form,Modal } from "antd";
import {MYTICKET} from './../../config/index';
import callApi from './../../until/callerApi';
class MyTicket extends Component {
    constructor(props)
    {
        super(props);
        this.state={
          inforChuyenbay:[],
          visible:false,
          chuyenBay:{},
          sanBayDi:{},
          sanBayDen:{},
          ngaydi:"",
          tgdi:""
        }
    }
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields(async (err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            let url=MYTICKET+"?mave="+values.id+"&email="+values.Email;
            await callApi("GET",url,null).then(res=>{
              if(res)
              {
                let tgdi=res.data.tgdi;
                //let temp=tgdi.split("T");
                //console.log("data",res.data);
                this.setState({
                  visible:true,
                  inforChuyenbay:res.data,
                  ngaydi:res.data.tgdi,
                  chuyenBay:res.data.chuyenbay,
                  sanBayDi:res.data.chuyenbay.sbdi,
                  sanBayDen:res.data.chuyenbay.sbden
                },()=>console.log(this.state.inforChuyenbay))
              }
              else
              {
                alert("Không tìm thấy mã đặt chỗ của bạn")
              }
            })
          }
        });
      };
    
      handleOk = async (e) => {
       
           this.setState({
            visible: false,})
        
      };
    
      handleCancel = (e) => {
        console.log(e);
        this.setState({
          visible: false,
        });
      };
  render() {
    let {inforChuyenbay,sanBayDi,sanBayDen,ngaydi,tgdi}=this.state;
    const { getFieldDecorator } = this.props.form;
    return (
      <div style={{ height: 500, paddingTop: 50 }}>
        <Row>
          <Col xs={7}></Col>
          <Col xs={10}>
          <Form labelCol={{ span: 5 }} wrapperCol={{ span: 12 }} onSubmit={this.handleSubmit}>
          <Form.Item label="Mã đặt chỗ">
            {getFieldDecorator('id', {
              rules: [{ required: true, message: 'Vui lòng nhập mã đặt chỗ' }],
            })(<Input />)}
          </Form.Item>
          <Form.Item label="Email">
            {getFieldDecorator('Email', {
              rules: [{ required: true, message: 'Vui lòng Email' }],
            })(<Input type='email' />)}
          </Form.Item>
          
          <Form.Item wrapperCol={{ span: 12, offset: 5 }}>
            <Button type="primary" htmlType="submit">
              Tìm
            </Button>
          </Form.Item>
        </Form>
          </Col>
          <Col xs={7}></Col>
        </Row>
        <br />
        <Modal
              title="Thông tin chuyến bay của bạn"
              visible={this.state.visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
            >
              <p>Chuyến bay : {(inforChuyenbay && inforChuyenbay.chuyenbay)?inforChuyenbay.chuyenbay.macb:"no data"}</p>
              <p>Từ : {sanBayDi.tp} </p>
              <p>Đến : {sanBayDen.tp}</p>
              <p>Ngày đi: {(inforChuyenbay && inforChuyenbay.chuyenbay)?inforChuyenbay.chuyenbay.ngaydi:"no data"}-{(inforChuyenbay && inforChuyenbay.chuyenbay)?(inforChuyenbay.chuyenbay.thangdi<10?"0"+inforChuyenbay.chuyenbay.thangdi:inforChuyenbay.chuyenbay.thangdi):""}-{(inforChuyenbay && inforChuyenbay.chuyenbay)?inforChuyenbay.chuyenbay.namdi:""}</p>
              <p>Giờ đi : {(inforChuyenbay && inforChuyenbay.chuyenbay)?(inforChuyenbay.chuyenbay.gioidi<10?"0"+inforChuyenbay.chuyenbay.gioidi:inforChuyenbay.chuyenbay.gioidi):""}:{(inforChuyenbay && inforChuyenbay.chuyenbay)?(inforChuyenbay.chuyenbay.phutdi<10?"0"+inforChuyenbay.chuyenbay.phutdi:inforChuyenbay.chuyenbay.phutdi):""}</p>

              <p>Giá vé: {(inforChuyenbay&& inforChuyenbay.hoadon)?inforChuyenbay.hoadon.thanhtien:"0"}</p>
            </Modal>
      </div>
    );
  }
}

export default Form.create()(MyTicket);
//vuduongcalvin@gmail.com
//619120