import React, { Component } from "react";
import { Layout } from "antd";
import HeaderLayout from "./header/index";
import routes from "./../routes";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
const { Footer } = Layout;
export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }
  render() {
    return (
      <Router>
        <Layout className="layout">
          <HeaderLayout/>
          {this.showContentMenu(routes)}
          <Footer>
          
            <img style={{width:'109%',marginLeft:-50}} src={process.env.PUBLIC_URL + "/assets/image/footer.PNG"} alt="thum"/>
          
          </Footer>
        </Layout>
      </Router>
    );
  }
  showContentMenu = (routes) => {
    var result = null;
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        );
      });
    }
    return <Switch>{result}</Switch>;
  };
}
