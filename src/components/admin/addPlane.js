import React, { Component } from "react";
import {
  Drawer,
  Form,
  Button,
  Col,
  Row,
  Input,
  Select,
  DatePicker,
  Icon,
  Table,
} from "antd";
import * as Types from "./../../constain/index";
import callApi from "./../../until/callerApi";
import { connect } from "react-redux";
import { actFetAirportRequest } from "./../../actions/index";
import Highlighter from "react-highlight-words";
import moment from 'moment';
import {GETALLPLANE} from './../../config/index'
const { Option } = Select;
class AddPlane extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      listAirport: [],
      searchText: "",
      searchedColumn: "",
      update:false,
      timeDi:new Date(),
      listPlane:[]
    };
  }
  async componentDidMount() {
    this.props.fetAllAirport();
    this.props.fetTurnPlane();
    await callApi("GET",GETALLPLANE,null).then(res=>{
      this.setState({
        listPlane:res.data
      })
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      // Should format date value before submit.
      const values = {
        ...fieldsValue,

        tgdi: fieldsValue["tgdi"].format("YYYY-MM-DDTHH:mm:ss"),
        tgden: fieldsValue["tgden"].format("YYYY-MM-DDTHH:mm:ss"),
      };
      console.log("Received values of form: ", values);
      if(this.state.update===false)
      {
        this.props.addTurnPlane(values);
      }
      else {
        this.props.updateTurnPlane(values);
      }
    });
  };
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };
  onUpdate=(record)=>{
    this.setState({
      visible:true,
      update:true,
    },()=>{
      this.props.form.setFieldsValue({
       macb:record.macb,
       mamb:record.mamb,
       masbdi:record.masbdi,
       masbden:record.masbden,
       tgdi:moment(record.tgdi),
       tgden:moment(record.tgden)
      })
    })
  }
  onDelete=(record)=>{
    var answer = window.confirm("Xóa")
    if(answer) {
      this.props.deleteTurnPlane(record);
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    //console.log("chuyen bay : ", this.props.listTurnPlane);
    const arrTurnPlane = [];
    let { listTurnPlane } = this.props;
    for (let i = 0; i < listTurnPlane.length; i++) {
      arrTurnPlane.push({
        macb: listTurnPlane[i].macb,
        tgdi: listTurnPlane[i].tgdi,
        tgden: listTurnPlane[i].tgden,
        masbdi:
          listTurnPlane[i] && listTurnPlane[i].sbdi
            ? listTurnPlane[i].sbdi.masb
            : "no",
        masbden:
          listTurnPlane[i] && listTurnPlane[i].sbden
            ? listTurnPlane[i].sbden.masb
            : "no",
        mamb:
          listTurnPlane[i] && listTurnPlane[i].maybay
            ? listTurnPlane[i].maybay.mamb
            : "no",
      });
    }
    const config = {
      rules: [
        { type: "object", required: true, message: "Please select time!" },
      ],
    };
    const rangeConfig = {
      rules: [
        { type: "array", required: true, message: "Please select time!" },
      ],
    };
    const columns = [
      {
        title: "Mã chuyến bay",
        dataIndex: "macb",
        key: "macb",
        ...this.getColumnSearchProps("macb"),
      },
      {
        title: "Thời gian đi",
        dataIndex: "tgdi",
        key: "tgdi",
        ...this.getColumnSearchProps("tgdi"),
      },
      {
        title: "Thời gian đến",
        dataIndex: "tgden",
        key: "tgden",
        ...this.getColumnSearchProps("tgden"),
      },
      {
        title: "Sân bay đi",
        dataIndex: "masbdi",
        key: "masbdi",
        ...this.getColumnSearchProps("masbdi"),
      },
      {
        title: "Sân bay đến",
        dataIndex: "masbden",
        key: "masbden",
        ...this.getColumnSearchProps("masbden"),
      },
      {
        title: "Mã máy bay",
        dataIndex: "mamb",
        key: "mamb",
        ...this.getColumnSearchProps("mamb"),
      },
      {
        title: "Hành động",
        key: "action",
        render: (text, record) => (
          <React.Fragment>
            <Button onClick={() => this.onUpdate(record)}>Sửa</Button>
            <Button onClick={() => this.onDelete(record)}>Xóa</Button>
          </React.Fragment>
        ),
      },

    ];
    return (
      <div>
        <Row style={{ paddingTop: 15 }}>
          <Col>
            <Button
              type="primary"
              shape="round"
              icon="plus-circle"
              size="large"
              onClick={() => {this.setState({ visible: true,update:false });
              this.props.form.setFieldsValue({
                macb:"",
                mamb:"",
                masbdi:"",
                masbden:"",
                tgdi:moment(),
                tgden:moment()
               })
            }}
            >
              Thêm mới
            </Button>
          </Col>
        </Row>
        <Row style={{ paddingTop: 15 }}>
          <Col>
            <Table columns={columns} dataSource={arrTurnPlane} />;
          </Col>
        </Row>

        <Drawer
          title={this.state.update===true?"Cập chật chuyến bay":"Thêm mới chuyến bay"}
          width={720}
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form layout="vertical" hideRequiredMark onSubmit={this.handleSubmit} ref={this.props.form}>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Mã chuyến bay">
                  {getFieldDecorator("macb", {
                    rules: [
                      { required: true, message: "Vui lòng nhâp mã chuyến bay" },
                    ],
                  })(<Input placeholder="Vui lòng nhâp mã chuyến bay" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Mã máy bay">
                  {getFieldDecorator("mamb", {
                    rules: [
                      { required: true, message: "Vui lòng nhập mã máy bay" },
                    ],
                  })(<Select placeholder="Vui lòng nhập mã máy bay">
                  {this.state.listPlane.map((air, index) => {
                    return (
                      <Option key={index} value={air.mamb}>
                        {air.tenmb}
                      </Option>
                    );
                  })}
                </Select>)}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="Thời gian đi">
                  {getFieldDecorator(
                    "tgdi",
                    config,
                  )(<DatePicker  showTime format="YYYY-MM-DD HH:mm:ss" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Thời gian đến">
                  {getFieldDecorator(
                    "tgden",
                    config,
                  )(<DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />)}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={12}>
                <Form.Item label="San bay di">
                  {getFieldDecorator("masbdi", {
                    rules: [
                      { required: true, message: "Vui lòng nhập sân bay" },
                    ],
                  })(
                    <Select placeholder="Vui lòng nhập sân bay">
                      {this.props.airPort.map((air, index) => {
                        return (
                          <Option key={index} value={air.masb}>
                            {air.tensb}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="San bay den">
                  {getFieldDecorator("masbden", {
                    rules: [
                      { required: true, message: "Vui lòng nhập sân bay" },
                    ],
                  })(
                    <Select placeholder="Vui lòng nhập sân bay">
                      {this.props.airPort.map((air, index) => {
                        return (
                          <Option key={index} value={air.masb}>
                            {air.tensb}
                          </Option>
                        );
                      })}
                    </Select>,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right",
              }}
            >
              <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                Hủy
              </Button>
              <Button type="primary" htmlType="submit">
                Lưu lại
              </Button>
            </div>
          </Form>
        </Drawer>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    airPort: state.listAirport,
    listTurnPlane: state.listTurnPlane,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    fetAllAirport: () => {
      dispatch(actFetAirportRequest());
    },
    fetTurnPlane: () => {
      dispatch({
        type: Types.FET_TURN_PLANE,
      });
    },
    addTurnPlane:(data)=>{
      dispatch({
        type:Types.ADD_TURN_PLANE,
        data:data
      })
    },
    updateTurnPlane:(data)=>{
      dispatch({
        type:Types.UPDATE_TURN_PLANE,
        data:data
      })
    },
    deleteTurnPlane:(data)=>{
      dispatch({
        type:Types.DELETE_TURN_PLANE,
        data:data
      })
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(AddPlane));
