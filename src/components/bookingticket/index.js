import React, { Component } from "react";
import {
  Carousel,
  Row,
  Col,
  Button,
  Form,
  Icon,
  DatePicker,
  Select,
  Radio,
} from "antd";
import "./index.scss";
import { connect } from "react-redux";
import { actFetAirportRequest } from "./../../actions/index";
import callApi from "./../../until/callerApi";
import axios from "axios";
const { Option } = Select;

class BookingTicket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueChkb: 1,
      addressFrom: "",
      addressTo: "2020",
      dateFrom: "",
      dateTo: "",
    };
  }
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    //return [year, month, day].join('-');
    return [day,month,year].join('');
  }
  onChangeFrom = (value) => {
    this.setState({
      addressFrom: value,
    });
  };
  onChangeTo = (value) => {
    this.setState({
      addressTo: value,
    });
  };
  onChangeTimeFrom = (date, dateString) => {
    this.setState({
      dateFrom: this.formatDate(dateString),
    });
  };
  onChangeTimeTo = (date, dateString) => {
    this.setState({
      dateTo: this.formatDate(dateString),
    });
  };
  onChangeChkb = (e) => {
    this.setState({
      valueChkb: e.target.value,
    });
  };
  onSubmit = (e) => {
    e.preventDefault();
    let { addressFrom, addressTo, dateFrom, dateTo,valueChkb }=this.state;
    if(valueChkb===1)
    {
      this.props.history.push("/displayticket/"+addressFrom+"/"+addressTo+"/"+dateFrom+"/"+dateFrom);
    }
    else
    {
      this.props.history.push("/displayticket/"+addressFrom+"/"+addressTo+"/"+dateFrom+"/"+dateTo);
    }
  };
  async componentDidMount() {
    await this.props.fetAllAirport();
    
  }
  render() {
    let { valueChkb } = this.state;
    let { airPort } = this.props;
    //console.log(airPort);
    return (
      <div className="divtong">
        <div>
          <div className="divFormSearch">
            <Button type="primary" className="titleAirplane">
              Tìm kiếm chuyến bay
            </Button>
            <div className="formInputSearch">
              <Form layout="inline" onSubmit={this.onSubmit}>
                <Form.Item label="Điểm đi">
                  <Select
                    showSearch
                    style={{ width: 150 }}
                    onChange={this.onChangeFrom}
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {airPort.map((air, index) => {
                      return (
                        <Option key={index} value={air.masb}>
                          {air.tensb}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
                <Form.Item label="Điểm đến">
                  <Select
                    showSearch
                    style={{ width: 150 }}
                    optionFilterProp="children"
                    onChange={this.onChangeTo}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {airPort.map((air, index) => {
                      return (
                        <Option key={index} value={air.masb}>
                          {air.tensb}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
                <Form.Item label="Ngày đi">
                  <DatePicker
                    style={{ width: 150 }}
                    onChange={this.onChangeTimeFrom}
                  />
                </Form.Item>
                <Form.Item label="Ngày về">
                  <DatePicker
                    style={{ width: 150 }}
                    disabled={valueChkb===0?false:true}
                    onChange={this.onChangeTimeTo}
                  />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    Tìm kiếm
                  </Button>
                </Form.Item>
              </Form>
            </div>
            <div className="radiobtn">
              <label>Hành trình : </label>
              <Radio.Group onChange={this.onChangeChkb} value={valueChkb}>
                <Radio value={1}>
                  <strong>Một chiều</strong>
                </Radio>
                <Radio value={0}>
                  <strong>Khứ hồi</strong>
                </Radio>
              </Radio.Group>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    airPort: state.listAirport,
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    fetAllAirport: () => {
      dispatch(actFetAirportRequest());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(BookingTicket);
