import React from "react";
import Home from "./components/home/index";
import PageDisplayTicket from "./components/pageDisplayTicket/index";
import MyTicket from "./components/myticket/index";
import BookSuccess from "./components/book-success/index";
import Login from "./components/admin/login";
import AddPlane from "./components/admin/addPlane";
const routes = [
  {
    path: "/",
    exact: true,
    main: ({ history }) => <Home history={history} />,
  },
  {
    path: "/displayticket/:airplanefrom/:airplaneto/:datefrom/:dateto",
    exact: false,
    main: ({ match, history }) => (
      <PageDisplayTicket match={match} history={history} />
    ),
  },
  {
    path: "/my-ticket",
    exact: false,
    main: () => <MyTicket />,
  },
  {
    path: "/book-success",
    exact: false,
    main: () => <BookSuccess />,
  },
  {
    path: "/admin",
    exact: false,
    main: ({ history }) => <Login history={history} />,
  },

  {
    path: "/addPlane",
    exact: false,
    main: () => <AddPlane />,
  },
];
export default routes;
